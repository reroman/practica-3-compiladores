%{
#include <stdlib.h>
#include "sintactico.tab.h"
%}

entero [[:digit:]]+
decimal {entero}\.{entero}|\.{entero}
mod [mM][oO][dD]

%%

[[:blank:]] {}

exit {
	exit( 0 );
}

clear {
	system( "clear" );
	printf( "Escribe una operación matemática\n" );
}

{entero} { 
	yylval.entero = atoi( yytext );
	return ENTERO;
}

{decimal} {
	yylval.real = atof( yytext );
	return DECIMAL;
}

"+"|"-"|"*"|"/"|"^"|"("|")"|"\n"|"," {
	return yytext[0];
}

{mod} {
	return MOD;
}

