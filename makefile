YACC := bison
YFLAGS := -d
LEX := flex
CC := gcc
LDLIBS := -lfl -lm
TARGET := Practica2
SOURCES := sintactico.tab.c lex.yy.c main.c
SINTACTICO := sintactico.y
LEXICO := lexico.l

$(TARGET): $(SOURCES)
	$(CC) -o $@ $^ $(LDLIBS)

sintactico.tab.c: $(SINTACTICO)
	$(YACC) $(YFLAGS) $<

lex.yy.c: $(LEXICO) $(SINTACTICO)
	$(LEX) $<

clean:
	 $(RM) $(wildcard $(join $(basename $(SINTACTICO)), .tab.*)) lex.yy.c
