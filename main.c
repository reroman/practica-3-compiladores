#include "sintactico.tab.h"
#include <stdio.h>

void yyerror( char *s )
{
	fprintf( stderr, "%s\n", s );
}

int main()
{
	printf( "Escribe una operacion matemática\n" );
	return yyparse();
}
