%{
#include <stdio.h>
#include <math.h>

int dividir_int( int a, int b );
double dividir_double( double a, double b );
int mod_int( int a, int b );
double mod_double( double a, double b );
int isValid = 1;
%}

%union{
	int entero;
	double real;
}

%token <entero> ENTERO
%token <real> DECIMAL
%token MOD 

%type <real> exp_real
%type <entero> exp_ent

%left '+' '-'
%left '*' '/'
%nonassoc NEG
%nonassoc POS
%right '^'

%start input

%%

input: 
	| input line
;

line: '\n'
	| exp_real '\n'	{ 
		if( isValid )
			printf( "Resultado real = %g\n", $1 ); }
	| exp_ent '\n'	{ 
		if( isValid ) 
			printf( "Resultado entero = %d\n", $1 ); }
;

exp_real: DECIMAL			{ $$ = $1; isValid = 1; }
	| '-' exp_real %prec NEG { $$ = -$2; isValid = 1; }
	| '+' exp_real %prec POS { $$ = $2; isValid = 1; }
	| exp_real '+' exp_real	{ $$ = $1 + $3; isValid = 1; }
	| exp_real '-' exp_real	{ $$ = $1 - $3; isValid = 1; }
	| exp_real '*' exp_real	{ $$ = $1 * $3; isValid = 1; }
	| exp_real '/' exp_real	{ $$ = dividir_double( $1, $3 ); }
	| exp_real '^' exp_real	{ $$ = pow( $1, $3 ); isValid = 1; }
	| MOD '(' exp_real ',' exp_real ')' { $$ = mod_double( $3, $5 ); }
	| '(' exp_real ')' { $$ = $2; isValid = 1; }

	| exp_ent '+' exp_real	{ $$ = $1 + $3; isValid = 1; }
	| exp_ent '-' exp_real	{ $$ = $1 - $3; isValid = 1; }
	| exp_ent '*' exp_real	{ $$ = $1 * $3; isValid = 1; }
	| exp_ent '/' exp_real	{ $$ = dividir_double( $1, $3 ); }
	| exp_ent '^' exp_real	{ $$ = pow( $1, $3 ); isValid = 1; }
	| MOD '(' exp_ent ',' exp_real ')' { $$ = mod_double( $3, $5 ); }

	| exp_real '+' exp_ent	{ $$ = $1 + $3; isValid = 1; }
	| exp_real '-' exp_ent	{ $$ = $1 - $3; isValid = 1; }
	| exp_real '*' exp_ent	{ $$ = $1 * $3; isValid = 1; }
	| exp_real '/' exp_ent	{ $$ = dividir_double( $1, $3 ); }
	| exp_real '^' exp_ent	{ $$ = pow( $1, $3 ); isValid = 1; }
	| MOD '(' exp_real ',' exp_ent ')' { $$ = mod_double( $3, $5 ); }
;

exp_ent: ENTERO 			{ $$ = $1; isValid = 1; }
	| '-' exp_ent %prec NEG { $$ = -$2; isValid = 1; }
	| '+' exp_ent %prec POS { $$ = $2; isValid = 1; }
	| exp_ent '+' exp_ent	{ $$ = $1 + $3; isValid = 1; }
	| exp_ent '-' exp_ent	{ $$ = $1 - $3; isValid = 1; }
	| exp_ent '*' exp_ent	{ $$ = $1 * $3; isValid = 1; }
	| exp_ent '/' exp_ent	{ $$ = dividir_int( $1, $3 ); }
	| exp_ent '^' exp_ent	{ $$ = pow( $1, $3 ); isValid = 1; }
	| MOD '(' exp_ent ',' exp_ent ')' { $$ = mod_int( $3, $5 ); }
	| '(' exp_ent ')' { $$ = $2; isValid = 1; }
;

%%

int dividir_int( int a, int b )
{
	if( !b ){
		fprintf( stderr, "No se puede dividir entre 0\n" );
		isValid = 0;
		return 0;
	}
	isValid = 1;
	return a / b;
}

double dividir_double( double a, double b )
{
	if( !b ){
		fprintf( stderr, "No se puede dividir entre 0\n" );
		isValid = 0;
		return NAN;
	}
	isValid = 1;
	return a / b;
}

int mod_int( int a, int b )
{
	if( !b ){
		fprintf( stderr, "No se puede dividir entre 0\n" );
		isValid = 0;
		return 0;
	}
	isValid = 1;
	return a % b;
}

double mod_double( double a, double b )
{
	if( !b ){
		fprintf( stderr, "No se puede dividir entre 0\n" );
		isValid = 0;
		return NAN;
	}
	isValid = 1;
	return fmod( a, b );
}
